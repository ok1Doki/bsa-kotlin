package edu.bsa.kotlin.ui.post_detail

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import edu.bsa.kotlin.R
import edu.bsa.kotlin.data.entity.CommentEntity
import edu.bsa.kotlin.data.entity.PostEntity
import edu.bsa.kotlin.data.entity.UserEntity
import edu.bsa.kotlin.ui.post_detail.comments.CommentListRecyclerAdapter
import kotlinx.android.synthetic.main.activity_post_detail.*
import kotlinx.android.synthetic.main.comment_item.view.*
import kotlinx.android.synthetic.main.post_item.view.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*
import kotlin.collections.ArrayList

class PostDetailActivity : AppCompatActivity() {

    private lateinit var postDetailViewModel: PostDetailViewModel

    private var adapter: CommentListRecyclerAdapter? = null
    private var post: PostEntity? = null
    private var commentsList: ArrayList<CommentEntity> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initRecyclerView()
        extractDataFromBundle()
        initViewModel()
        loadData()
    }

    private fun initRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        post_comments.layoutManager = layoutManager
        adapter = CommentListRecyclerAdapter(commentsList)
        post_comments.adapter = adapter
    }

    private fun extractDataFromBundle() {
        post = intent
            .getBundleExtra("Bundle")
            .getParcelable<PostEntity>("post")
        supportActionBar?.title = post?.title
    }

    private fun initViewModel() {
        postDetailViewModel =
            ViewModelProviders.of(this,
                post?.let { PostDetailViewModelFactory(it.userId, it._id) })
                .get(PostDetailViewModel::class.java)
    }

    private fun loadData() {
        postDetailViewModel.getComments().observe(this, Observer {
            commentsList.clear()
            commentsList.addAll(it)
            post_detail_comments_counter.text = "Comments: ${commentsList.size}"
            adapter?.notifyDataSetChanged()

            post_detail_title.text = post!!.title
            post_detail_body.text = post!!.body

            val timestamp = LocalDateTime.parse(post!!.createdAt)
            val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
            post_detail_timestamp.text = formatter.format(timestamp)
            progress.visibility = View.GONE

        })

        postDetailViewModel.getUser().observe(this, Observer {
            if (it != null) {
                post_detail_author_name.text = it.name + " " + it.surname
                post_detail_author_email.text = it.email
                post_detail_author_about.text = it.about

                Glide.with(this).load(it.avatar)
                    .circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(post_detail_author_avatar);

                post_detail_author_card.setOnClickListener {
                    if (post_detail_author_about.visibility == View.GONE) {
                        post_detail_author_about.visibility = View.VISIBLE
                        post_detail_author_email.visibility = View.VISIBLE
                        post_detail_author_avatar.visibility = View.VISIBLE
                    } else {
                        post_detail_author_about.visibility = View.GONE;
                        post_detail_author_email.visibility = View.GONE;
                        post_detail_author_avatar.visibility = View.GONE;
                    }
                }
            }
        })

        Glide.with(this).load(post!!.imgUrl)
            .fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(post_detail_img);


    }
}