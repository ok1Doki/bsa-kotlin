package edu.bsa.kotlin.ui.posts_list

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.bsa.kotlin.R
import edu.bsa.kotlin.data.Repository
import edu.bsa.kotlin.data.entity.PostEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_posts.*

class PostsActivity : AppCompatActivity() {

    private val postRepo = Repository.of<PostEntity>()

    private var adapter: PostsItemAdapter? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var postsList: ArrayList<PostEntity>? = arrayListOf()

    private fun initRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        list.layoutManager = layoutManager
        adapter = PostsItemAdapter(this, postsList!!)
        list.adapter = adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts)
        initRecyclerView()

        getAllPosts()
    }

    private fun getAllPosts() {
        var disposable: Disposable = postRepo.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { this.handleResponse(it) },
                { it.printStackTrace() }
            )
        compositeDisposable?.add(disposable)
    }

    private fun handleResponse(posts: List<PostEntity>) {
        postsList?.clear()
        postsList?.addAll(posts)
        adapter?.notifyDataSetChanged()
    }
}