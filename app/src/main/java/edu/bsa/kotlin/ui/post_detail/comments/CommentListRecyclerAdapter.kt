package edu.bsa.kotlin.ui.post_detail.comments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import edu.bsa.kotlin.R
import edu.bsa.kotlin.data.entity.CommentEntity
import kotlinx.android.synthetic.main.comment_item.view.*

class CommentListRecyclerAdapter(
    var comments: List<CommentEntity>
) : RecyclerView.Adapter<CommentListRecyclerAdapter.CommentsViewHolder>() {

    inner class CommentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.comment_item, parent, false)
        return CommentsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
        val currentComment = comments[position]
        holder.itemView.comment_author.text = currentComment.name
        holder.itemView.comment_body.text = currentComment.body
        holder.itemView.comment_timestamp.text = currentComment.createdAt

        Glide.with(holder.itemView.context).load(currentComment.avatar)
            .circleCrop()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(holder.itemView.comment_avatar);
    }

}