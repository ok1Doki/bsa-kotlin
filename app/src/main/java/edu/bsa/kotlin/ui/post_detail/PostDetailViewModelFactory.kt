package edu.bsa.kotlin.ui.post_detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PostDetailViewModelFactory(val userId: String, val postId: String) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PostDetailViewModel(userId, postId) as T
    }
}