package edu.bsa.kotlin.ui.posts_list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import edu.bsa.kotlin.R
import edu.bsa.kotlin.data.entity.PostEntity
import edu.bsa.kotlin.ui.post_detail.PostDetailActivity
import kotlinx.android.synthetic.main.post_item.view.*


class PostsItemAdapter(
    private val context: Context,
    var postItems: List<PostEntity>
) : RecyclerView.Adapter<PostsItemAdapter.PostsViewHolder>() {

    inner class PostsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.post_item, parent, false)
        return PostsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return postItems.size
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val currentPost = postItems[position]

        holder.itemView.setOnClickListener {
            val toPass = Bundle()
            toPass.putParcelable("post", currentPost)

            val intent = Intent(this.context, PostDetailActivity::class.java)
            intent.putExtra("Bundle", toPass)
            context.startActivity(intent)
        }

        holder.itemView.title.text = currentPost.title
        holder.itemView.timestamp.text = currentPost.createdAt
        holder.itemView.txtStatusMsg.text = currentPost.body

        Glide.with(holder.itemView.context).load(currentPost.avatar)
            .circleCrop()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(holder.itemView.profilePic);

        Glide.with(holder.itemView.context).load(currentPost.imgUrl)
            .fitCenter()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(holder.itemView.postImage);

    }

}
