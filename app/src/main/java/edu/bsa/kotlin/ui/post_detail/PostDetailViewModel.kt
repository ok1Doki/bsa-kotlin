package edu.bsa.kotlin.ui.post_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import edu.bsa.kotlin.data.Repository
import edu.bsa.kotlin.data.entity.CommentEntity
import edu.bsa.kotlin.data.entity.UserEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class PostDetailViewModel(userId: String, postId: String) : ViewModel() {

    fun getUser(): LiveData<UserEntity> = loadUserFromRepository

    fun getComments(): LiveData<List<CommentEntity>> = loadCommentsFromRepository

    private val loadUserFromRepository: LiveData<UserEntity> by lazy {
        val userRepo = Repository.of<UserEntity>()
        val liveData = MutableLiveData<UserEntity>()
        userRepo.query()
            .where(Repository.ID, userId).findFirst()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { liveData.value = it },
                { it.printStackTrace() }
            )
        return@lazy liveData
    }

    private val loadCommentsFromRepository: LiveData<List<CommentEntity>> by lazy {
        val commentRepo = Repository.of<CommentEntity>()
        val liveData = MutableLiveData<List<CommentEntity>>()
        commentRepo.query()
            .where(Repository.POST_ID, postId).findAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { liveData.value = it },
                { it.printStackTrace() }
            )
        return@lazy liveData
    }
}




