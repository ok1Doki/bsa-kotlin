package edu.bsa.kotlin.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class UserEntity(
    @PrimaryKey
    var _id: String,
    var name: String,
    var surname: String,
    var email: String? = null,
    var about: String? = null,
    var avatar: String? = null
) {
    override fun hashCode(): Int {
        return _id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return if (other is UserEntity) other._id == _id else false
    }
}