package edu.bsa.kotlin.data.api

import edu.bsa.kotlin.data.entity.*
import io.reactivex.Observable
import retrofit2.http.*

interface Api {

    @GET("posts")
    fun getPosts(): Observable<List<PostEntity>>

    @GET("posts")
    fun getPostById(
        @Query("_id") postId: String?
    ): Observable<PostEntity>


    @GET("posts")
    fun getPostByUserId(
        @Query("userId") userId: String?
    ): Observable<List<PostEntity>>



    @GET("comments")
    fun getCommentsByPostId(
        @Query("postId") postId: String?
    ): Observable<List<CommentEntity>>



    @GET("users")
    fun getUsers(): Observable<List<UserEntity>>


    @GET("users")
    fun getUserById(
        @Query("_id") userId: String?
    ): Observable<List<UserEntity>>

}