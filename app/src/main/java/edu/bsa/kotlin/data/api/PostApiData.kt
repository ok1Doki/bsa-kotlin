package edu.bsa.kotlin.data.api

import edu.bsa.kotlin.data.DataSource
import edu.bsa.kotlin.data.Repository
import edu.bsa.kotlin.data.entity.PostEntity
import io.reactivex.Completable
import io.reactivex.Observable

class PostApiData : DataSource<PostEntity> {

    private val api: Api = ApiService.create(Api::class.java)

    override fun getAll(): Observable<List<PostEntity>> {
        return api.getPosts()
    }

    override fun getAll(query: DataSource.Query<PostEntity>): Observable<List<PostEntity>> {
        return when {

            query.has(Repository.ID) -> query.get(Repository.ID)?.let { id ->
                api.getPostById(id).map { listOf(it) }
            } ?: throw IllegalArgumentException("Unsupported query $query for PostEntity")

            query.has(Repository.USER_ID) -> query.get(Repository.USER_ID)?.let { userId ->
                api.getPostByUserId(userId)
            } ?: throw IllegalArgumentException("Unsupported query $query for PostEntity")

            else -> throw IllegalArgumentException("Unsupported query $query for PostEntity")
        }
    }

    override fun saveAll(list: List<PostEntity>): Observable<List<PostEntity>> {
        TODO("not implemented")

    }

    override fun removeAll(list: List<PostEntity>): Completable {
        TODO("not implemented")

    }

    override fun removeAll(): Completable {
        TODO("not implemented")
    }
}