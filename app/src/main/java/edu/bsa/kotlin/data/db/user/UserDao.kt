package edu.bsa.kotlin.data.db.user

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import edu.bsa.kotlin.data.entity.UserEntity
import io.reactivex.Maybe

@Dao
abstract class UserDao {

    @Query("SELECT * FROM users")
    abstract fun getAll(): Maybe<List<UserEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(users: List<UserEntity>)

    @Delete
    abstract fun deleteAll(users: List<UserEntity>)

    @Query("DELETE FROM users")
    abstract fun deleteAll()

    @RawQuery
    abstract fun rawQuery(query: SupportSQLiteQuery): Maybe<List<UserEntity>>
}