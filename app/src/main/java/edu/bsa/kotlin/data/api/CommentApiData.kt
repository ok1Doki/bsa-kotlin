package edu.bsa.kotlin.data.api

import edu.bsa.kotlin.data.DataSource
import edu.bsa.kotlin.data.Repository
import edu.bsa.kotlin.data.entity.CommentEntity
import io.reactivex.Completable
import io.reactivex.Observable

class CommentApiData : DataSource<CommentEntity> {

    private val api: Api = ApiService.create(Api::class.java)

    override fun getAll(): Observable<List<CommentEntity>> {
        TODO("not implemented")
    }

    override fun getAll(query: DataSource.Query<CommentEntity>): Observable<List<CommentEntity>> {
        return when {
            query.has(Repository.POST_ID) -> query.get(Repository.POST_ID)?.let { userId ->
                api.getCommentsByPostId(userId)
            } ?: throw IllegalArgumentException("Unsupported query $query for CommentEntity")

            else -> throw IllegalArgumentException("Unsupported query $query for CommentEntity")
        }
    }

    override fun saveAll(list: List<CommentEntity>): Observable<List<CommentEntity>> {
        TODO("not implemented")
    }

    override fun removeAll(list: List<CommentEntity>): Completable {
        TODO("not implemented")
    }

    override fun removeAll(): Completable {
        TODO("not implemented")
    }
}