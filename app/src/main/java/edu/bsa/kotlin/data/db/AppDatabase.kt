package edu.bsa.kotlin.data.db

import android.content.Context
import androidx.room.*
import edu.bsa.kotlin.data.api.*
import edu.bsa.kotlin.data.db.comment.CommentDao
import edu.bsa.kotlin.data.db.user.UserDao
import edu.bsa.kotlin.data.entity.*

import java.util.*

@Database(
        entities = [
            UserEntity::class,
            PostEntity::class,
            CommentEntity::class
        ],
        version = 2,
        exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getUserDao(): UserDao

    abstract fun getPostDao(): PostDao

    abstract fun getCommentDao(): CommentDao

    companion object {

        private const val DB_NAME = "bsa_app.db"

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase = INSTANCE
            ?: synchronized(this) {
            INSTANCE
                ?: buildDatabase(
                    context.applicationContext
                ).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context, AppDatabase::class.java,
                    DB_NAME
                )
                        .fallbackToDestructiveMigration()
                        .build()
    }
}

class DateConverter {

    @TypeConverter
    fun toDate(value: Long?) = value?.let { Date(value) }

    @TypeConverter
    fun toLong(value: Date?) = value?.time
}