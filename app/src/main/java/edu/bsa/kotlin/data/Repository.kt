package edu.bsa.kotlin.data

import edu.bsa.kotlin.App.Companion.isNetworkAvailable
import edu.bsa.kotlin.data.api.ApiData
import edu.bsa.kotlin.data.db.DbData
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

object Repository {

    const val ID = "_id"
    const val USER_ID = "userId"
    const val POST_ID = "postId"

    inline fun <reified Entity : Any> of(): Repo<Entity> {
        return Repo<Entity>(ApiData.of(Entity::class), DbData.of(Entity::class))
    }

    fun clearDatabase(): Completable {
        return Completable.fromCallable { DbData.clearDb() }
            .subscribeOn(Schedulers.io())
    }
}

class Repo<Entity : Any>(val api: DataSource<Entity>, val db: DataSource<Entity>) :
    DataSource<Entity> {

    override fun getAll(): Observable<List<Entity>> {
        return Observable.concatArrayEager(
            db.getAll().subscribeOn(Schedulers.io()),
            Observable.defer {
                if (isNetworkAvailable())
                    api.getAll().subscribeOn(Schedulers.io())
                        .flatMap { l ->
                            db.removeAll()
                                .andThen(db.saveAll(l))
                        }
                else
                    Observable.empty()
            }.subscribeOn(Schedulers.io())
        )
    }


    override fun getAll(query: DataSource.Query<Entity>): Observable<List<Entity>> {
        return Observable.concatArrayEager(
            db.getAll(query).subscribeOn(Schedulers.io()),
            Observable.defer {
                if (isNetworkAvailable())
                    api.getAll(query).subscribeOn(Schedulers.io())
                        .flatMap { l ->
                            db.getAll(query)
                                .flatMapCompletable { old -> db.removeAll(old) }
                                .andThen(db.saveAll(l))
                        }
                else
                    Observable.empty()
            }.subscribeOn(Schedulers.io())
        )
    }

    override fun saveAll(list: List<Entity>): Observable<List<Entity>> {
        return db.saveAll(list)
    }


    override fun removeAll(list: List<Entity>): Completable {
        return db.removeAll(list)
    }

    override fun removeAll(): Completable {
        return db.removeAll()
    }


}