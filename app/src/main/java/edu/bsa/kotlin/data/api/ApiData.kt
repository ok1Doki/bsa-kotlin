package edu.bsa.kotlin.data.api

import edu.bsa.kotlin.data.DataSource
import edu.bsa.kotlin.data.entity.*
import kotlin.reflect.KClass

object ApiData {

    fun <Entity : Any> of(clazz: KClass<*>): DataSource<Entity> {
        return when (clazz) {
            UserEntity::class -> UserApiData()
            PostEntity::class -> PostApiData()
            CommentEntity::class -> CommentApiData()
            else -> throw IllegalArgumentException("Unsupported data type")
        } as DataSource<Entity>
    }
}