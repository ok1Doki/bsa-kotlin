package edu.bsa.kotlin.data.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "comments")
data class CommentEntity(
    @PrimaryKey
    var _id: String,
    var postId: String,
    var name: String,
    var body: String,
    var createdAt: String
) {

    @Ignore
    var avatar: String = ""

    fun toMap(): Map<String, String> {
        return mapOf(
            "postId" to postId,
            "name" to name,
            "createdAt" to createdAt,
            "body" to body
        )
    }

    override fun hashCode(): Int {
        return _id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return if (other is CommentEntity) other._id == _id else false
    }
}